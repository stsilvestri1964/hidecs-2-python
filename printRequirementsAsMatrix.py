import requirements as reqlib
import matrixRepresentation as mreps
# reqs,adj = reqlib.fromJson("requirementsNSOF.json")
reqs,adj = reqlib.fromJson("requirementsC&P.json")
verts, edges = reqlib.dataFromAdjacencies(adj)
matrix = mreps.matrixRepresentation(verts,edges)
matrix = mreps.makeSymmetrical(matrix)
mreps.printUnicode(verts,matrix)