
#These are modified now so they can deal with partially built trees.
#in that case dangling nodes that are not leaves are treated as if they would be

def calculateMaxDepth(tree, node, depth):
    depth += 1;
    # if tree.isNodeTerminal(node):
    if tree.isNodeLeaf(node):
        return depth
    else:
        depthA = calculateMaxDepth(tree, tree.getChildA(node), depth)
        depthB = calculateMaxDepth(tree,tree.getChildB(node), depth)
        return depthA if depthA > depthB else depthB

def maxDepth(tree,node):
    return calculateMaxDepth(tree,node, 0)

# It includes both leaves and terminal nodes (not assigned yet)
def calculateNumberOfTeminals(tree,node,numTerminals):
    # if tree.isNodeTerminal(node):
    if tree.isNodeLeaf(node):
        return numTerminals + 1
    else:
        numTerminalsA = calculateNumberOfTeminals(tree,tree.getChildA(node),numTerminals);
        numTerminalsB = calculateNumberOfTeminals(tree,tree.getChildB(node),numTerminals);
        return numTerminalsA + numTerminalsB;

def numOfTerminals(tree,node):
    return calculateNumberOfTeminals(tree,node, 0)


def calculateNumberOfLeaves(tree,node,numLeaves):
    if tree.isNodeLeaf(node):
        return numLeaves + 1
    else:
        numLeavesA = calculateNumberOfLeaves(tree,tree.getChildA(node),numLeaves);
        numLeavesB = calculateNumberOfLeaves(tree,tree.getChildB(node),numLeaves);
        return numLeavesA + numLeavesB;

def numOfLeaves(tree,node):
    return calculateNumberOfLeaves(tree,node, 0)



# Depending of when they are visited, this can be used for laying them 
# out.One can think that the first leaf visited is the leftmost, and then 
# add a value to posX (and return it)

def layoutNode(tree, node, posX, level, leafFunc, nodeFunc, edgeFunc):
    nodePos = 0
    # if tree.isNodeTerminal(node):
    if tree.isNodeLeaf(node):
        posX += 1
        nodePos = posX

        if tree.isNodeLeaf(node):
            leafFunc(node, posX, level)
        else:
            nodeFunc(node,nodePos,level)

    else:
        aPos=layoutNode(tree,tree.getChildA(node), posX,level+1, leafFunc, nodeFunc, edgeFunc)
        posX = aPos[1]
        #pos could be set to a new posX, then there will never overlap
        #nodePos =  ++posX;
        bPos=layoutNode(tree,tree.getChildB(node), posX,level+1, leafFunc, nodeFunc, edgeFunc)
        posX = bPos[1]

        #just a way of calculating the pos
        nodePos = (aPos[0]+ bPos[0])/2.0

        #do all drawing
        nodeFunc(node,nodePos,level)
        edgeFunc(nodePos, aPos[0], level)
        edgeFunc(nodePos, bPos[0], level)

    return (nodePos,posX);

def layouToStringVertical(tree,node):
    elemWidth = 4
    elemHeight = 2
    height = maxDepth(tree,node)


    #create string raster
    vizWidth= elemWidth * numOfTerminals(tree,node) + 10 # add some padding 
    vizHeight = elemHeight * height + 10
    viz =  [[' ' for j in range(vizWidth)] for i in range(vizHeight)]

    def leafF(node,posX,level):
        nonlocal viz
        insX = int(posX*elemWidth)
        insY = level*elemHeight

        viz[insY][insX] = '|'  
        #write leaf
        s = tree.getLeaf(node)
        
        for el in s:
            insY += 1
            stv = str(el)
            viz[insY] = viz[insY][:insX] + list(stv) + viz[insY][insX + len(stv):]


    def nodeF(node,posX,level): #not doing anything with node, but it could
        nonlocal viz
        insX = int(posX*elemWidth)
        insY = level*elemHeight
        viz[insY][insX] ='|'
        viz[insY + 1][insX] ='|'

    def edgeF(posXA,  posXB, level):
        nonlocal viz
        insXA = int(posXA*elemWidth)
        insXB = int(posXB*elemWidth)
        insY = level*elemHeight + 1
        
        if posXA>posXB: #to left
            size = insXA - insXB - 1
            line =  ['_' for i in range(size)]
            locX = insXA - size  
            viz[insY] = viz[insY][:locX] + line + viz[insY][insXA:]
        else:
            size = insXB - insXA - 1
            line =  ['_' for i in range(size)]
            locX = insXA+1
            viz[insY] = viz[insY][:locX] + line + viz[insY][insXB:]  


    layoutNode(tree,node,0,0,leafF,nodeF,edgeF)
    s = ""
    for row in viz:
        s += "".join(row) + "\n"

    return s


def layouToStringHorizontal(tree,node):
    elemWidth = 4
    elemHeight = 2
    width = maxDepth(tree,node)

    #create string raster
    vizWidth= elemWidth * width + 20 # add some padding 
    vizHeight = elemHeight * numOfLeaves(tree,node) +10
    viz =  [[' ' for j in range(vizWidth)] for i in range(vizHeight)]

    def leafF(node,posY,level):
        nonlocal viz
        insX = level * elemWidth
        insY = posY * elemHeight

        viz[insY][insX] = '-'  
        #write leaf
        s = str(tree.getLeaf(node))
        lines = list('-' for i in range(elemWidth))
        lines.append(' ')
        insX += 1

        viz[insY] = viz[insY][:insX] + lines + list(s) + viz[insY][insX + len(s):]


    def nodeF(node,posY,level): #not doing anything with node, but it could
        nonlocal viz
        insX = (level+1) * elemWidth 
        insY = int(posY * elemHeight)
        lines = list('-' for i in range(elemWidth-1))
        lines.append('|')

        viz[insY] = viz[insY][:insX-elemWidth] + lines + viz[insY][insX:]

    def edgeF(posYA,  posYB, level):
        nonlocal viz
        insX = (level+1) * elemWidth -1
        insYA = int(posYA * elemHeight)
        insYB = int(posYB * elemHeight)

        if posYA>posYB: #to left
            size = insYA - insYB -1 
            locY = insYA - size  
            for yp in range(locY,insYA):
                viz[yp][insX] = '|'
        else:
            size = insYB - insYA 
            locY = insYA+1
            for yp in range(locY,insYB):
                viz[yp][insX] = '|'


    layoutNode(tree,node,0,0,leafF,nodeF,edgeF)
    s = ""
    for row in viz:
        s += "".join(row) + "\n"
        
    return s

