
# Python implementations of functions described in:
# Research Report R62-2
# HIDECS 2: A COMPUTER PROGRAM FOR THE HIERARCHICAL DECOMPOSITION 
# OF A SET WHICH HAS AN ASSOCIATED LIEAR GRAPH
# by Christopher Alexander, Society of Fellows, Harvard University
# and Marvin L. Manheim, Department of Civil Engineering, M.I.T.
# Publication No. 160, June 1962

import math
import random
show = False
#This it the strength of the partition in terms of the least amount of information trasmitted

import time #just used if show is True

def calculateInfo(TOTAL,ORDER,RR,M,N):
	NSQ1 = ORDER * (ORDER-1) /2.0 # The max number of links possible.
	# if TOTAL == NSQ1: # A complete graph. According to SMRMN page 36.
	# 	return float("-inf") #make sure that it will be minimum
	#written slighltly different (page 26)
	denom = RR - M*N * TOTAL/NSQ1

	if  M*N == 0:
		raise RuntimeError(f" M*N is 0, with M: {M}, N: {N}")
	if M*N == NSQ1:
		raise RuntimeError(f" NSQ1 - M*N is 0, with M: {M}, N: {N},, ORDER: {ORDER}, RR {RR}")

	info = denom*denom/(M*N * (NSQ1 - M*N))
	return info if denom >=0 else -info  #STR^2 with sign. Page 26

def calculateTotal(graph,vSet): #the number of edges linking points that are both in vSet
	total = 0
	for e in graph: 
		total += 1 if e[0] in vSet and e[1] in vSet else 0
	return total

def calculateRR(graph,vSet,vSubset):
	rr = 0
	for e in graph:
		if e[0] not in vSet or e[1] not in vSet: 
			raise RuntimeError(f"Graph includes links ot nodes outside vSet in calculateRR")	
		caseCut  = 1 if e[0] in vSubset else 0
		caseCut += 1 if e[1] in vSubset else 0
		rr += 1 if caseCut == 1 else 0 
	return rr

 #graph, vSet a set of vertices, vSubset as a set of vertices in vSet
def calculateGraphInfo(graph,vSet,vSubset):
	order = len(vSet)
	subOrder = len(vSubset)
	rr = calculateRR(graph, vSet, vSubset)
	total = calculateTotal(graph,vSet)
	return calculateInfo(total,order, rr, order-subOrder, subOrder)


def genetateStartOfPath(vSet): #part of SMRMN in Alexander (C8)
	subSet = set()
	choiceSet=list(vSet)
	while not subSet: #make sure it is not empty
		for i in range(random.randint(0,len(choiceSet)-1)): #prevent same length
			subSet.add(random.choice(choiceSet))

	# return tuple(subSet) #return as tuple
	return subSet


def addLoop(graph, vSet, vSub):
	if show: # a bit of a hack here to print process
		bestInfo = calculateGraphInfo(graph,vSet,vSub)
		def calculateVal(el):
			nonlocal bestInfo
			info = calculateGraphInfo(graph,vSet,vSub | {el})
			printFormatted((vSet,vSub | {el}, ' ' + str(el) +' ->', info, 'information value improved.' if info < bestInfo else ''))
			bestInfo = info if info < bestInfo else bestInfo
			return info
		selected = min([el for el in vSet if el not in vSub], key = calculateVal)
	else:
		selected = min([el for el in vSet if el not in vSub], key = lambda el : calculateGraphInfo(graph,vSet,vSub | {el}))

	return selected if calculateGraphInfo(graph,vSet,vSub | {selected}) < calculateGraphInfo(graph,vSet,vSub) else None


def subtractLoop(graph, vSet, vSub):
	if show: # a bit of a hack here to print process
		bestInfo = calculateGraphInfo(graph,vSet,vSub)
		def calculateVal(el):
			nonlocal bestInfo
			info = calculateGraphInfo(graph,vSet,vSub-{el})
			printFormatted((vSet,vSub-{el}, ' ' + str(el) +' ->', info, 'information value improved.' if info < bestInfo else ''))
			bestInfo = info if info < bestInfo else bestInfo
			return info
		selected = min(vSub, key = calculateVal)
	else:
		selected = min(vSub, key = lambda el : calculateGraphInfo(graph,vSet,vSub-{el}))

	return selected if calculateGraphInfo(graph,vSet,vSub-{selected}) < calculateGraphInfo(graph,vSet,vSub) else None






# it should be called from SMRMN the number of samples set
def hillClimb(graph, vSet, vSub):
	#bestInfo = calculateGraphInfo(graph,vSet,vSubset)
	if show:
		# printFormatted ('\nHill climbing through a potential subdivision:')
		printFormatted((vSet,vSub,'',calculateGraphInfo(graph,vSet,vSub), 'Initial partition to test.'))
		printFormatted('')
		# time.sleep(0.5)

	# vSub = list(vSubset) #make sure it is a list
	while True:

		# if show:
			# printFormatted((vSet,vSub,'<--|',calculateGraphInfo(graph,vSet,vSub),'Testing moving nodes from right to left.'))

		toAdd = addLoop(graph,vSet,vSub) if len(vSub) < len(vSet) - 1 else None
		if toAdd:
			vSub.add(toAdd)

		if show:
			if not toAdd:
				# printFormatted('No improvement found. Using initial partition')
				printFormatted((vSet,vSub,'',calculateGraphInfo(graph,vSet,vSub),'No improvement, use previous partition:'))
			else:
				printFormatted((vSet,vSub,'',calculateGraphInfo(graph,vSet,vSub),'Improvement found, use new partition:'))


			# printFormatted((vSet,vSub,'|-->',calculateGraphInfo(graph,vSet,vSub),'Testing moving nodes from left to right.'))

		toSubtract = subtractLoop(graph,vSet,vSub) if len(vSub)>1 else None
		if toSubtract:
			vSub.remove(toSubtract)

		if not toAdd and not toSubtract: #if it could not improve
			break

		if show:
			if not toSubtract:
				printFormatted((vSet,vSub,'',calculateGraphInfo(graph,vSet,vSub),'No improvement, use previous partition:'))
			else:
				printFormatted((vSet,vSub,'',calculateGraphInfo(graph,vSet,vSub),'Improvement found, use new partition:'))


		# printFormatted(vSet,vSub,'_',calculateGraphInfo(graph,vSet,vSub))
	if show:
		printFormatted('')
		printFormatted((vSet,vSub,'',calculateGraphInfo(graph,vSet,vSub),'best partition found by hill-climber:'),flush=True)
		# printFormatted('----------------------------------------------------------',flush=True)

	return vSub

#run the hillclimber LATIS times, and choose best result§
def SMRMN(graph, vSet, LATIS):

	#check if graph (vset) is complete:
	total = calculateTotal(graph,vSet)
	order = len(vSet)
	nsq1 = order * (order-1) /2.0
	if total == nsq1 : #is complete
		return vSet

	if order == 2:
		# This means that they are two independent, unlinked nodes (otherwise 
		# total == nsq1 == 1 would have fulfilled the previous condition)
		# so divide by two (or it will give a divide by zero error in
		# calculating the Info value, M*N - NSQ1 ==0)
		# return vSet[:-1]
		return {vSet.pop()} # since it has two elements any would do.
	t = 0
	info = float('inf') #a max value
	vSub = []
	while t < LATIS:
		
		nvSub = genetateStartOfPath(vSet)
		nvSub = hillClimb(graph,vSet,nvSub)
		nInfo = calculateGraphInfo(graph,vSet,nvSub)

		
		if nInfo < info:
			vSub = nvSub
			info = nInfo
		t+=1

	return vSub

# This is a function to  reduce the size of the graph (the list of edges) in each recursion
# so it only includes edges on vSet. It is simply to optimise the searches and calculations,
# so it is not necessary to include the whole full graph
def subGraph(graph, vSet):
	return [e for e in graph if e[0] in vSet and e[1] in vSet]


def subdivide(graph, vSet, attach, makeNode, makeLeaf): #addBranch needs to return a node

	def subdivide(graph, vSet):
		graph = subGraph(graph, vSet) #getSubgraph, optimisation
		subSet = SMRMN(graph,vSet,10)
		if subSet == vSet:	
			return makeLeaf(sorted(vSet)) #to print them sorted
			# return makeLeaf(vSet)


		complement = set([el for el in vSet if el not in subSet])

		node = makeNode()
		attach(node,subdivide(graph, subSet))
		attach(node,subdivide(graph, complement))

		return node

	if vSet is not set:
		vSet = set(vSet)
	return subdivide(graph,set(vSet))



# def printFormatted(vSet, vSub, sepString, info, note = '', flush=False):
# def printFormatted(vSet, vSub, sepString, info, note = '', flush=False):
def printFormatted(val, flush=False):

	if type(val) is tuple: #it does not use named tupples to keep all simple, 
		if type(val[0]) is set and type(val[1]) is set and type(val[2]) is str and type(val[3]) is float and type(val[4]) is str:
			stringTupple = (str(val[1]),str(val[0]-val[1]),val[2],val[3],val[4])
			printFormatted.outBuf.append(stringTupple)
		else: 
			raise RuntimeError("Trying to add tupple of unknown form in printFormatted.")
	elif type(val) is str:
		printFormatted.outBuf.append(val)


	if flush:

		tuples = list(filter(lambda el : type(el) is tuple, printFormatted.outBuf))
		strings = list(filter(lambda el : type(el) is str, printFormatted.outBuf))

	
		left  = max(len(ele[0]) for ele in tuples) 
		right = max(len(ele[1]) for ele in tuples) 
		largestNote = max(len(ele[4]) for ele in tuples) 
		largestString = max(len(string) for string in strings) 
		first = max(largestNote,largestString) + 2 # for nicer formating
		

		sortv = sorted({ele[3] for ele in tuples})
		rank ={ sortv[i] : i for i in range(len(sortv))}

		spacing = 8

		# maxLength = left+right +spacing + len(rank) + 2 + 9 #2 for the | and 9 for the val
		maxLength = len(rank)*2 + 3 + 9

		setValsSeparator = 5

		#print columns:
		columns =('requirements set A', '', 'requirements set B', 'Information value')
		# print(first*' ', end ='')
		#make sure there is enough space:
		left =  	len(columns[0]) if len(columns[0]) > left else left
		spacing =	len(columns[1]) if len(columns[1]) > spacing else spacing
		right = 	len(columns[2]) if len(columns[2]) > right else right

		margin0 = int(first + left - len(columns[0]))
		print(margin0*' ' + columns[0], end ='')
		margin1 = int((spacing - len(columns[1]))/2)
		print( margin1*' ' + columns[1] + margin1*' ', end ='')
		margin2 = int(right - len(columns[2]))
		print(columns[2] + margin2*' ', end ='')
		margin3 = int(maxLength +  - len(columns[3]))
		print(setValsSeparator*' ' + columns[3] + margin3*' ')

		hSeparator = '-'*(first + left + spacing + right + setValsSeparator + maxLength)
		print(hSeparator)

		for line in printFormatted.outBuf:
			if type(line) is str:
				print(line)
			else:
				vSubStr, vCompStr,sep,infoval, note = line
				print( note + ' '*(first - len(note)), end = '')
				# margin = ' ' * int((spacing-len(sep))/2)
				# separator = margin + sep + margin
				# separator += '' if len(separator) == spacing else ' '

				#right justified
				margin = ' ' * int(spacing-len(sep) -1)
				separator = margin + sep +' '
				# separator += '' if len(separator) == spacing else ' '

				print( ' '*(left-len(vSubStr)) + vSubStr, end = '')
				print(separator, end = '')
				print(vCompStr + ' '*(right-len(vCompStr)),end = '')
				
				valStr = setValsSeparator*' ' + '|' + 2*rank[infoval]*' ' + '|'
				# valStr += ' ' + (' ' if infoval > 0 else '') + f'{infoval:.6f}'
				# valStr += ' ' + (' ' if infoval > 0 else '') + f'{infoval:.6f}'
				valStr += ((len(rank)-rank[infoval])*2+1)*' ' + (' ' if infoval >= 0 else '') + f'{infoval:.6f}'
				# print (valStr, end = '')
				print (valStr)

			# print((maxLength-len(valStr))*' ' + note)

		print(hSeparator)


		printFormatted.outBuf = [] #clean the buffers


printFormatted.outBuf = []

