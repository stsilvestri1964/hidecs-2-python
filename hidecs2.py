# Python implementations of functions described in:
# Research Report R62-2
# HIDECS 2: A COMPUTER PROGRAM FOR THE HIERARCHICAL DECOMPOSITION 
# OF A SET WHICH HAS AN ASSOCIATED LIEAR GRAPH
# by Christopher Alexander, Society of Fellows, Harvard University
# and Marvin L. Manheim, Department of Civil Engineering, M.I.T.
# Publication No. 160, June 1962

import math
import random

def calculateInfo(TOTAL,ORDER,RR,M,N):
	NSQ1 = ORDER * (ORDER-1) /2.0 # The max number of links possible.
	# if TOTAL == NSQ1: # A complete graph. According to SMRMN page 36.
	# 	return float("-inf") #make sure that it will be minimum
	#written slighltly different (page 26)
	denom = RR - M*N * TOTAL/NSQ1

	if  M*N == 0:
		raise RuntimeError(f" M*N is 0, with M: {M}, N: {N}")
	if M*N == NSQ1:
		raise RuntimeError(f" NSQ1 - M*N is 0, with M: {M}, N: {N},, ORDER: {ORDER}, RR {RR}")

	info = denom*denom/(M*N * (NSQ1 - M*N))
	return info if denom >=0 else -info  #STR^2 with sign. Page 26

def calculateTotal(graph,vSet): #the number of edges linking points that are both in vSet
	total = 0
	for e in graph: 
		total += 1 if e[0] in vSet and e[1] in vSet else 0
	return total

def calculateRR(graph,vSet,vSubset):
	rr = 0
	for e in graph:
		if e[0] not in vSet or e[1] not in vSet: 
			raise RuntimeError(f"Graph includes links ot nodes outside vSet in calculateRR")	
		caseCut  = 1 if e[0] in vSubset else 0
		caseCut += 1 if e[1] in vSubset else 0
		rr += 1 if caseCut == 1 else 0 
	return rr

#graph, vSet a set of vertices, vSubset as a set of vertices in vSet
def calculateGraphInfo(graph,vSet,vSubset):
	order = len(vSet)
	subOrder = len(vSubset)
	rr = calculateRR(graph, vSet, vSubset)
	total = calculateTotal(graph,vSet)
	return calculateInfo(total,order, rr, order-subOrder, subOrder)


def genetateStartOfPath(vSet): #part of SMRMN in Alexander (C8)
	subSet = set()
	choiceSet=list(vSet)
	while not subSet: #make sure it is not empty
		for i in range(random.randint(1,len(choiceSet)-1)): #prevent same length
			subSet.add(random.choice(choiceSet))
	return subSet


def addLoop(graph, vSet, vSub):
	selected = min( [el for el in vSet if el not in vSub], key = lambda el : calculateGraphInfo(graph,vSet,vSub | {el}))
	return selected if calculateGraphInfo(graph,vSet,vSub | {selected}) < calculateGraphInfo(graph,vSet,vSub) else None


def subtractLoop(graph, vSet, vSub):
	selected = min(vSub, key = lambda el : calculateGraphInfo(graph,vSet,vSub - {el}))
	return selected if calculateGraphInfo(graph,vSet,vSub-{selected}) < calculateGraphInfo(graph,vSet,vSub) else None


def hillClimb(graph, vSet, vSub): #vSub is the TSET in Alexander's description.
	while True:
		toAdd = addLoop(graph,vSet,vSub) if len(vSub) < len(vSet) - 1 else None
		if toAdd:
			vSub.add(toAdd)

		toSubtract = subtractLoop(graph,vSet,vSub) if len(vSub)>1 else None
		if toSubtract:
		 	vSub.remove(toSubtract)

		if not toAdd and not toSubtract: #if it could not improve
			break
	return vSub


#run the hillclimber LATIS times, and choose best result§
def SMRMN(graph, vSet, LATIS):
	#check if graph (vset) is complete:
	total = calculateTotal(graph,vSet)
	order = len(vSet)
	nsq1 = order * (order-1) /2.0
	if total == nsq1 : #is complete
		return vSet

	if order == 2:
		return {vSet.pop()} # since it has two elements any would do.
	t = 0
	info = float('inf') #a max value
	vSub = []
	while t < LATIS:	
		nvSub = genetateStartOfPath(vSet)
		nvSub = hillClimb(graph,vSet,nvSub)
		nInfo = calculateGraphInfo(graph,vSet,nvSub)
	
		if nInfo < info:
			vSub = nvSub
			info = nInfo
		t+=1

	return vSub

def subGraph(graph, vSet):
	return [e for e in graph if e[0] in vSet and e[1] in vSet]


def subdivide(graph, vSet, attach, makeNode, makeLeaf): #addBranch needs to return a node

	def subdivide(graph, vSet):
		graph = subGraph(graph, vSet) #getSubgraph, optimisation
		# subSet = SMRMN(graph,vSet,10)
		subSet = SMRMN(graph,vSet,2*len(vSet)+10) #scaling of LATIS depending on size
		if subSet == vSet:	
			return makeLeaf(sorted(vSet)) #to print them sorted

		complement = set([el for el in vSet if el not in subSet])

		node = makeNode()
		attach(node,subdivide(graph, subSet))
		attach(node,subdivide(graph, complement))

		return node
	if vSet is not set:
		vSet = set(vSet)

	return subdivide(graph,set(vSet))
