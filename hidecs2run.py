import hidecs2
import requirements
import tree
import treeDrawing


def calculateprogramfrom(data):

	#to print §
	def attach(parent,child):
		tree.addChild(parent,child)
		print("\n\n\n")
		out = treeDrawing.layouToStringVertical(tree,parent)
		print(out)


	hidecs2.show = True
	# program = hidecs2.subdivide(data[1], data[0], tree.addChild, tree.newNode,tree.newLeaf)
	program = hidecs2.subdivide(data[1], data[0], attach, tree.newNode,tree.newLeaf)
	hidecs2.show = False

	print(treeDrawing.layouToStringVertical(tree,program))
	

if __name__ == '__main__':
	req,adj = requirements.fromJson("requirementsNSOF.json")
	# req,adj = requirements.fromJson("requirementsC&P.json")
	#req,adj = requirements.fromJson("requirementsGraphTest.json")
	program = calculateprogramfrom(requirements.dataFromAdjacencies(adj))
