
def newNode():
	return ['n',newLeaf(""),newLeaf("")]

def newLeaf(val):
	return ['l',val]

def setChildA(node,elem):
		node[1] = elem

def setChildB(node,elem):
		node[2] = elem


#check if it has default value or not
#it is not set if the chid is a leaf and the leaf is an empty string

def isStump(node):
	return isNodeLeaf(node) and not getLeaf(node)

def hasChildA(node): 
	return not isStump(node[1])

def hasChildB(node):
	return not isStump(node[2])

def addChild(node,elem): # a practical way of not caring for a or b
	if not hasChildA(node):
		setChildA(node,elem)
	elif not hasChildB(node):
		setChildB(node,elem)
	else:
		raise RuntimeError("Trying to add a child to a filled node.")


def isNodeLeaf(node):
	return node[0] =='l' if node else False

def getChildA(node):
	if node:
		return node[1]
	return None

def getChildB(node):
	if node:
		return node[2]
	return None

def getLeaf(node): #it works only if it is a leaf.
	return node[1]



# Draw --------------------------------------
# A Bsic drawing/printing for testing. More complex forms of drawing
# Are given in treeDrawing.py

def drawNode(node,depth):
	if node and len(node)<=1:
		print("empty")
		return
	print('\t'*depth + '[')


	if isNodeLeaf(node):
		print('\t'*depth + str(getLeaf(node)))
		return
	elif isNodeTerminal(node):
		print('\t'*depth + "AB")
	else:
		drawNode(getChildA(node),depth + 1)
		drawNode(getChildB(node),depth + 1)

	print('\t'*depth + ']')

def drawTree(tree):
	drawNode(tree,0)



