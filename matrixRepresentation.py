#A graph represented by edges (links) can be passed here and it will return a matrix representation of the graph
#this is for undirected graphs. Alexander uses a rectangular matrix. In reality, because his graphs are undirected,
# a trinagular matrix could be used more efficiently

def matrixRepresentation(vertices,edges): #this allows to have matrices for subgraphs
	matSize = len(vertices)
	matrix = [[0 for c in range(matSize)] for r in range(matSize)] 
	#a map of edge number to index in matrix:
	vertextoIndex = dict(zip(vertices,range(matSize)))
	for edge in edges:
		v0,v1 = edge
		if v0 in vertextoIndex and v1 in vertextoIndex:
			matrix[vertextoIndex[v0]][vertextoIndex[v1]] = 1
	return matrix

def makeSymmetrical(matrix):
	nm = [x[:] for x in matrix]
	for r in range(len(matrix)):
		for c in range(len(matrix)):
			nm[r][c] = nm[c][r] = max(matrix[r][c],matrix[c][r])
	return nm

def printUnicode(vertices,matrix):
	longest = len(str(max(vertices)))
	# spacing = longest + 1
	spacing = 2

	nsp = spacing-1
	for  i in range(longest):
		print(' '*(longest+1), end = '') #to match the spaces of the horizontal numbers 
		for j in range(0,len(matrix)):
			nstr = str(vertices[j])
			nstr = (longest-len(nstr))*' ' + nstr;
			print(nstr[i] + ' '*nsp, end = '')
		print() 

	ct=0
	for row in matrix:
		vertexName = str(vertices[ct])
		nsp = longest-len(vertexName)
		print( ' '*nsp + vertexName + ' ', end = '')
		ct+=1
		for l in row:
			s = '\u2588' if l > 0 else ' ' #2588: full block
			# s = '\u2589' if l > 0 else ' '	 #2589: 7/8 block, left
			print(s + ' '*(spacing-1),  end = '')
		print()